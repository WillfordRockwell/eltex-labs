#include <unistd.h> 
#include <sys/types.h> 
#include <stdio.h> 
#include <stdlib.h>
#include <fcntl.h>
int main(int argc, char *argv[]){
	FILE *hdl1;
	FILE *hdl2;
	int total = 0;
	int matr[3];
	int vect[3];
	int i = 0;
	int stren = 0;
	/*if (argc < 2) {
		printf("Need more arguments to process!\n");
		return (0);
	}*/
	if (hdl1 = fopen("matrix.txt", "r")){
		while (!feof(hdl1)) {
			fscanf(hdl1, "%d %d %d", &matr[i], &matr[i + 1], &matr[i + 2]);
			if ((char)fgetc(hdl1) == 0x0A) stren++; 
			if (stren == atoi(argv[1])) break;
		}
		fclose(hdl1);
		if (hdl2 = fopen("vector.txt", "r")){
			while (!feof(hdl2)) {
				fscanf(hdl2, "%d\n%d\n%d\n", &vect[i], &vect[i + 1], &vect[i + 2]);
			}
			fclose(hdl2);
		}
		else {
			printf("Can't open file 2!\n");
			return (0);
		}
		total = 0;
		for (i = 0; i < 3; i++){
			total += matr[i] * vect[i];
		}
		printf("(PlD: %d), Result = %d\n", getpid(), total);
	}
	else {
		printf("Can't open file 1!\n");
		return (0);
	}
	return (total);
}
//---------------------------------------------------------------------------
