#include <sys/types.h> 
#include <errno.h>
#include <sys/stat.h> 
#include <wait.h> 
#include <fcntl.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h>
#include <string.h>
//---------------------------------------------------------------------------
int main(int argc, char *argv[]){
	int i, pid[5], status, stat, total;
	total = 0;
	char arg[5] = "";
	printf("Begin\n");
	for (i = 0; i < 5; i++){
		pid[i] = fork();
		if(pid[i] == 0){
			sprintf(arg,"%d", i + 1);
			if(execl("./lab31", "lab31", arg, NULL) < 0){
				printf("%d's errno = %s\n", i,  strerror(errno));
				printf("Error to start %d process\n", i);
				exit(-1);
			}
			else {
				printf("Processing started (pid = %d)\n", pid[i]);
			}
			
		}
	}
	sleep(1);
	for(i = 0; i < 5; i++){
		status = waitpid(pid[i], &stat, 0);
		if(pid[i] == status) {
			printf("Proc %d is done, result = %d\n", i, WEXITSTATUS(stat));
			total += WEXITSTATUS(stat);
		}
	}
	printf("Total = %d\n", total);
	return total;
}
//---------------------------------------------------------------------------
