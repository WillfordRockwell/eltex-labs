#include <errno.h>
#include <unistd.h> 
#include <string.h>
#include <sys/types.h> 
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h> 
#include <stdlib.h>
#include <fcntl.h>
#include <sys/sem.h>

//---------------------------------------------------------------------------

int main(int argc, char *argv[]){ //semID, shmID, noSem
	
	FILE *hdl1;
	FILE *hdl2;
	int semID = atoi(argv[1]);
	int shmID = atoi(argv[2]);
	int noSem = atoi(argv[3]);

	struct sembuf sembf, *sembuff;
	sembuff = &sembf;

	int num = 0;
	int total = 0;
	int matr[3];
	int vect[3];
	int i = 0;
	int stren = 0;
	int* ptr;

	sembuff->sem_num = noSem;
	sembuff->sem_op = -1;
	sembuff->sem_flg = 0;

	if(semop(semID, sembuff, 1) == 0){//try to lock noSem sem
		printf("(PID: %d)Locked %d's sem\n", getpid(), noSem);
		ptr = (int*)shmat(shmID, NULL, 0);//attach ShM
		num = *ptr;//Read ShM to num
	} else {
		printf("(PID: %d)Can't lock %d's sem, errno = %s\n", getpid(), noSem, strerror(errno));
		exit(-1);
	}
	if ((hdl1 = fopen("matrix.txt", "r")) != NULL){
		while (!feof(hdl1)) {
			fscanf(hdl1, "%d %d %d", &matr[i], &matr[i + 1], &matr[i + 2]);
			if ((char)fgetc(hdl1) == 0x0A) stren++; 
			if (stren == num) break;
		}
		fclose(hdl1);
		if ((hdl2 = fopen("vector.txt", "r")) != NULL){
			while (!feof(hdl2)) {
				fscanf(hdl2, "%d\n%d\n%d\n", &vect[i], &vect[i + 1], &vect[i + 2]);
			}
			fclose(hdl2);
		}
		else {
			printf("(PID: %d)Can't open file 2!\n", getpid());
			exit (-1);
		}
		total = 0;
		for (i = 0; i < 3; i++){
			total += matr[i] * vect[i];
		}
		printf("(PlD: %d)Result = %d\n", getpid(), total);
	}
	else {
		printf("(PID: %d)Can't open file 1!\n", getpid());
		exit (-1);
	}
	
	*ptr = total;//Write total to ShM
	shmdt(ptr);//detach ShM
	sembuff->sem_op = 1;
	if(semop(semID, sembuff, 1) == 0){//Rise noSem sem
		printf("(PID: %d)Rised %d's sem\n", getpid(), noSem);
	} else {
		printf("(PID: %d)Can't lock %d's sem, errno = %s\n", getpid(), noSem, strerror(errno));
		exit(-1);
	}
	return 0;
}
//---------------------------------------------------------------------------
