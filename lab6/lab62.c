#include <sys/types.h> 
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <errno.h>
#include <sys/stat.h> 
#include <wait.h> 
#include <fcntl.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h>
#include <string.h>
#include "semun.c"

//---------------------------------------------------------------------------

int main(){
	int key = ftok("/etc/fstab", getpid());
	int semID = semget(key, 5, 0660 | IPC_CREAT);
	printf("SemID = %d\n", semID);
	union semun arg;
	unsigned short arr[]={1,1,1,1,1};
	arg.array=arr;
	semctl(semID, 0, SETALL, arg);

	struct sembuf sembf,*sembuff;
	sembuff = &sembf;
	int shmID[5];
	int i;
	int val;
	int pid[5], total = 0;
	printf("Begin\n");
	int n = 5, er;
	char buf0[80];
	char buf[8];
	char buf1[8];
	char buf2[8];
	int* ptr;
	for (i = 0; i < n; i++){
		if((shmID[i] = shmget(IPC_PRIVATE, sizeof(int), 0660 | IPC_CREAT)) != -1){
			printf("ShM with ID %d was created\n", shmID[i]);
		} else {
			printf("ShM was NOT created\n");
			exit(-1);
		}
		ptr = shmat(shmID[i], NULL, 0);//atach ShM
		val = i+1;
		*ptr = val;//Write val to ShM
		shmdt(ptr);//detach ShM

		sembuff->sem_num = i;
		sembuff->sem_op = 1;
		sembuff->sem_flg = 0;

		if(semop(semID, sembuff, 1) == 0){//rise i sem
			printf("Parent rised %d sem\n", i);
		} else {
			printf("Parent can't rise %d's sem, errno = %s\n", i, strerror(errno));
			exit (-1);
		}
		sprintf(buf0,"./lab61 ");
		sprintf(buf, "%d", semID);
		sprintf(buf1, "%d", shmID[i]);
		sprintf(buf2, "%d", i);
		pid[i] = fork();
		if(pid[i] == 0)	execl("./lab61", "lab61", buf, buf1, buf2, NULL);//semID, shmID, i
	}
	sleep(2);
	for(i = 0; i < n; i++){
		sembuff->sem_num = i;
		sembuff->sem_op = -1;
		sembuff->sem_flg = 0;
		
		if(semop(semID, sembuff, 1) == 0){//try to lock i sem
			printf("Parent locked %d sem\n", i);
			ptr = (int*)shmat(shmID[i], NULL, 0);//atach ShM
			val = *ptr;//Read result to val
			shmdt(ptr);//detach ShM
		} else {
			printf("Parent can't lock %d sem, errno = %s\n", i, strerror(errno));
			exit(-1);
		}
		total+=val;
		sembuff->sem_op = 1;
		semop(semID, sembuff, 1);
		shmdt(ptr);
		if(shmctl(shmID[i], IPC_RMID, NULL) == 0){//delete i ShM
			printf("ShMID %d was deleted\n", shmID[i]);
		} else {
			printf("ShMID %d was not deleted, errno = %s\n", shmID[i], strerror(errno));
		}
	}
	if(semctl(semID, 0, IPC_RMID, NULL) == 0){//delete sems
		printf("semID deleted\n");
	} else {
		printf("semID not deleted, errno = %s\n", strerror(errno));
		exit(-1);
	}
	printf("Total = %d\n", total);
	return total;
}
//---------------------------------------------------------------------------
