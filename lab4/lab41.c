#include <unistd.h> 
#include <string.h>
#include <sys/types.h> 
#include <stdio.h> 
#include <stdlib.h>
#include <fcntl.h>
int main(int argc, char *argv[]){
	FILE *hdl1;
	FILE *hdl2;
	char *read = argv[1];
	char *write = argv[2];
	printf("(PID: %d)read = %s\n",getpid(), read);
	printf("(PID: %d)write = %s\n", getpid(), write);
	FILE *fifoR; //to Read From Parent
	FILE *fifoW; //to Write to Parent
	int num = 0;
	int total = 0;
	int matr[3];
	int vect[3];
	int i = 0;
	char line[80];
	char *ln = line;
	int stren = 0;

	if (argc < 3 ) {
		printf("Need more arguments to process!\n");
		return (0);
	}
	printf("(PID: %d)Readty to read from FIFO\n", getpid());
	if((fifoR = fopen(read, "r")) ==  NULL){
		printf("(PID: %d)FIFO to read from parent is NOT  opened\n", getpid());
		return 0;
	}
	fgets(line, sizeof(line) - 1, fifoR);
	printf("(PID: %d)line = %s, (PID: %d)sizeof = %lu\n", getpid(), line, getpid(), strlen(line));
	num = atoi(line);
	printf("(PID: %d)num = %d\n", getpid(), num);
	if ((hdl1 = fopen("matrix.txt", "r")) != NULL){
		while (!feof(hdl1)) {
			fscanf(hdl1, "%d %d %d", &matr[i], &matr[i + 1], &matr[i + 2]);
			if ((char)fgetc(hdl1) == 0x0A) stren++; 
			if (stren == num) break;
		}
		fclose(hdl1);
		if ((hdl2 = fopen("vector.txt", "r")) != NULL){
			while (!feof(hdl2)) {
				fscanf(hdl2, "%d\n%d\n%d\n", &vect[i], &vect[i + 1], &vect[i + 2]);
			}
			fclose(hdl2);
		}
		else {
			printf("Can't open file 2!\n");
			return (0);
		}
		total = 0;
		for (i = 0; i < 3; i++){
			total += matr[i] * vect[i];
		}
		printf("(PlD: %d)Result = %d\n", getpid(), total);
	}
	else {
		printf("Can't open file 1!\n");
		return (0);
	}
	sprintf(ln, "%d\n", total);
	if((fifoW = fopen(write, "w")) == NULL){
		printf("(PID: %d)FIFO to write to parent is NOT opened\n", getpid());
		return 0;
	}
	fputs(line, fifoW);
	printf("(PID: %d)writed to parent %s, sizeof %lu\n", getpid(), line, strlen(line));
	return 0;
}
//---------------------------------------------------------------------------
