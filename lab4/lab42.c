#include <sys/types.h> 
#include <errno.h>
#include <sys/stat.h> 
#include <wait.h> 
#include <fcntl.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h>
#include <string.h>
#define FILEMODE (S_IRUSR | S_IWUSR)
//---------------------------------------------------------------------------
struct FIFOs {
	char name[80];
	FILE *file;
};

int main(){
	struct FIFOs FIFOes[10];
	
	int i;
	for(i = 0; i < 10; i++){
		sprintf(FIFOes[i].name, "/tmp/fifo.%d", (i + 1));
		printf("FIFOes %d = %s\n", i, FIFOes[i].name);
	}
	int pid[5], status, stat, total = 0;
	char buf[80];
	char arg[3] = "";
	printf("Begin\n");
	int n = 5;
	for (i = 0; i < n; i++){
		if((mkfifo(FIFOes[2 * i].name, FILEMODE) >= 0) && (errno != EEXIST)){
			printf("FIFO %d created\n", (2 * i));
		}
		else{
			printf("FIFO %d  NOT created or already exist\n", (2 * i));
		}
		if((mkfifo(FIFOes[2 * i + 1].name, FILEMODE) >= 0) && (errno != EEXIST)){
			printf("FIFO %d created\n", (2 * i + 1));
		} else {
			printf("FIFO %d  NOT created or already exist\n", (2 * i + 1));
		}
		pid[i] = fork();
		sleep(1);
		if(pid[i] == 0){
			if(execl("./lab41", "lab41", FIFOes[2 * i].name, FIFOes[2 * i + 1].name, NULL) < 0){
				printf("%d's errno = %s\n", i,  strerror(errno));
				printf("Error to start %d process\n", i);
				exit(-1);
			}
			else {
				printf("Processing started (pid = %d)\n", pid[i]);
			}
			
		}
		else {
			if((FIFOes[2 * i].file = fopen(FIFOes[2 * i].name, "w")) == NULL){
				printf("Parent cant open %d FIFO\n", (2 * i));
			} else {
				sprintf(arg, "%d", (i + 1));
				fputs(arg, FIFOes[2 * i].file);
				printf("Bytes %d (arg = %s) send to child = %lu\n", i, arg, strlen(arg));
				fclose(FIFOes[2 * i].file);
			}
		}
	}
	for(i = 0; i < n; i++){
		if((FIFOes[2 * i + 1].file = fopen(FIFOes[2 * i + 1].name, "r")) == NULL){
			printf("Parent cant open %d FIFO\n", (2 * i + 1));
		} else {
			fgets(buf, sizeof(buf) - 1, FIFOes[2 * i + 1].file);
			printf("%d buf = %s\n", i, buf);
			total += atoi(buf);
		}
		status = waitpid(pid[i], &stat, 0);
		fclose(FIFOes[2 * i + 1].file);
		unlink(FIFOes[2 * i].name);
		unlink(FIFOes[2 * i + 1].name);
	}
	printf("Total = %d\n", total);
	return total;

}
//---------------------------------------------------------------------------
