#include <sys/types.h> 
#include <sys/ipc.h>
#include <pthread.h>
#include <errno.h>
#include <sys/stat.h> 
#include <wait.h> 
#include <fcntl.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h>
#include <string.h>

//---------------------------------------------------------------------------

pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;

int num = 0;

void *thread(void *arg){
	
	FILE *hdl1, *hdl2;
        int total = 0;
        int matr[3];
        int vect[3];
        int i = 0;
        int stren = 0;
        printf("(Tno: %lu)Num = %d\n", (unsigned long)pthread_self(), num);
        if ((hdl1 = fopen("matrix.txt", "r")) != NULL){
                while (!feof(hdl1)) {
                        fscanf(hdl1, "%d %d %d", &matr[i], &matr[i + 1], &matr[i + 2]);
                        if ((char)fgetc(hdl1) == 0x0A) stren++;
                        if (stren == num) break;
                }
		pthread_mutex_unlock(&m);
                fclose(hdl1);
                if ((hdl2 = fopen("vector.txt", "r")) != NULL){
                        while (!feof(hdl2)) {
                                fscanf(hdl2, "%d\n%d\n%d\n", &vect[i], &vect[i + 1], &vect[i + 2]);
                        }
                        fclose(hdl2);
                }
                else {
                        printf("(Tno: %lu)Can't open file 2!\n", (unsigned long)pthread_self());
                        exit (-1);
                }
                for (i = 0; i < 3; i++){
                        total += matr[i] * vect[i];
                }
                printf("(Tno: %lu)Result = %d\n", (unsigned long)pthread_self(), total);
        }
        else {
                printf("(Tno: %lu)Can't open file 1!\n", (unsigned long)pthread_self());
                exit (-1);
        }
	return total;
}

int main(){
	int i, total = 0;
	int res;
	const int n = 5;
	pthread_t tid[n];
	int ret;

	printf("Begin\n");
	for(i = 0; i < n; i++){
		res = 0;
		pthread_mutex_lock(&m);//UNLOCK
		num = i + 1;
		if((ret = pthread_create(&tid[i], NULL, thread, NULL)) == 0){
			printf("%d's thread created\n", i);
		} else {
			printf("Errno %d's to create thread = %s", i, strerror(ret));
		}
	}

	for(i = 0; i < n; i++){
		pthread_join(tid[i], &res);
		total+=res;
		printf("Tno: %d's returned = %d\n", i, res);
	}
	printf("Total = %d\n", total);
	return total;
}
//---------------------------------------------------------------------------
