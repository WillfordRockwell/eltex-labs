// Client.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <stdio.h>
#include <winsock2.h>
#include <windows.h>

#pragma argsused

int _tmain(int argc, _TCHAR* argv[]){//IP Сервера, порт сервера
	if (argc != 3){
		printf("Not enough param's use: Client.exe IPServer PortServer\n");
		return -1;
	}
	//Начинаем инициализировать шиндовс сокет

	struct WSAData WS;
	if (FAILED(WSAStartup(/*MAKEWORD(2, 0)*/ 0x202, (WSADATA *)&WS))){
		//error
		printf("Client can NOT initialize WSAStartup, error: %d\n", WSAGetLastError());
		return -2;
	}
	//Создаем TCP сокет

	SOCKET sockTCP;
	if ((sockTCP = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == INVALID_SOCKET){
		//error
		printf("Client can NOT create socket, error: %d\n", WSAGetLastError());
	}
	//Заполняем информацию о Сервере и подключаемся к нему

	sockaddr_in server_addr;
	ZeroMemory(&server_addr, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.S_un.S_addr = inet_addr(argv[1]);
	server_addr.sin_port = htons(atoi(argv[2]));
	if (connect(sockTCP, (sockaddr *)&server_addr, sizeof(server_addr)) == SOCKET_ERROR){
		//error
		printf("Client can NOT connect socket to Server, error: %d\n", WSAGetLastError());
		return -3;
	}
	//Отправляем первое сообщение Серверу 

	char buff[80] = "HelloServer5";
	if (send(sockTCP, (char *) &buff[0], strlen(buff), 0) == SOCKET_ERROR){
		//error
		printf("Client can NOT send message '%s' to Server, error: %d\n", buff, WSAGetLastError());
		return -3;
	}
	else {
		printf("Client sended message '%s' to Server\n", buff);
	}
	//Получаем ответ от Сервера

	ZeroMemory(&buff, sizeof(buff));
	int recvBytes = 0;
	if (recvBytes = recv(sockTCP, (char *) &buff[0], 1024, 0) == SOCKET_ERROR){
		//error
		printf("Client can NOT recv message from Server, error: %d\n", WSAGetLastError());
		return -3;
	}
	else {
		printf("Client recv message '%s' from Server\n", buff);
	}
	//Присылаем Серверу количество строк

	//char buff[80];
	//sprintf(buff, "%s", argv[3]);

	ZeroMemory(&buff, sizeof(buff));
	sprintf(buff, "%d", 5);
	if (send(sockTCP, (char * )&buff[0], strlen(buff), 0) == SOCKET_ERROR){
		//error
		printf("Client can NOT send message '%s' to Server, error: %d\n", buff, WSAGetLastError());
		return -3;
	}
	else {
		printf("Client sended message '%s' code: %d to Server\n", buff, (int)buff[0]);
	}
	//Получаем ответ с решением от Сервера

	ZeroMemory(&buff, sizeof(buff));
	/*int*/ recvBytes = 0;
	if (recvBytes = recv(sockTCP, (char *) &buff[0], 1024, 0) == SOCKET_ERROR){
		//error
		printf("Client can NOT recv message from Server, error: %d\n", WSAGetLastError());
		return -3;
	}
	else {
		printf("Client recv message TOTAL: '%s' from Server\n", buff);
	}
	//Отправляем сообщение Серверу о конце

	ZeroMemory(&buff, sizeof(buff));
	sprintf(buff, "ByeServer");
	if (send(sockTCP, (char *)&buff[0], strlen(buff), 0) == SOCKET_ERROR){
		//error
		printf("Client can NOT send message '%s' to Server, error: %d\n", buff, WSAGetLastError());
		return -3;
	}
	else {
		printf("Client sended message '%s' to Server\n", buff);
	}

	closesocket(sockTCP);

	return 0;
}