#include <errno.h>
#include <unistd.h> 
#include <string.h>
#include <sys/types.h> 
#include <netinet/in.h>
#include <stdio.h> 
#include <stdlib.h>
#include <sys/socket.h>

//---------------------------------------------------------------------------

int nclients = 0;

//---------------------------------------------------------------------------

void error(const char *msg){
    perror(msg);
    exit(1);
}

//---------------------------------------------------------------------------


int solver(int count, int pid){
	printf("(PID : %d)SOLVER\n", pid);
	FILE *hdl1;
        FILE *hdl2;

	int num = 0;
        int total = 0;
        int matr[3];
        int vect[3];
        int i = 0;
        int stren;

        for(num = 1; num <= count; num++){//may be threads?
		printf("(PID : %d)loop\n", pid);
		stren = 0;
                if ((hdl1 = fopen("matrix.txt", "r")) != NULL){
                        printf("(PID : %d)hdl1\n", pid);
			while (!feof(hdl1)) {
                                fscanf(hdl1, "%d %d %d", &matr[0], &matr[1], &matr[2]);
                                if ((char)fgetc(hdl1) == 0x0A) stren++;
                                if (stren == num) break;
                        }
                        fclose(hdl1);
                        if ((hdl2 = fopen("vector.txt", "r")) != NULL){
				printf("(PID : %d)hdl2\n", pid);
                                while (!feof(hdl2)) fscanf(hdl2, "%d\n%d\n%d\n", &vect[0], &vect[1], &vect[2]);
                                fclose(hdl2);
                        } else {
                                printf("(PID : %d)can't open file 2!\n", pid);
                                exit (-1);
                        }
                        for (i = 0; i < 3; i++) total += matr[i] * vect[i];
                        printf("(PID : %d)result of %d' iteration = %d\n", pid, num, total);
                } else {
                        printf("(PID: %d)can't open file 1!\n", pid);
                        exit (-1);
                }
        }
	return total;
}

//---------------------------------------------------------------------------

void printusers(){
	if(nclients > 0){ 
		printf("%d user on-line\n",nclients);
	} else {
 		printf("No User on line\n");
	}
}

//---------------------------------------------------------------------------

void DOIT(int sock){
	int recvBytes = 0;
	int num;
	char buff[20 * 1024];
	bzero((char *)&buff, sizeof(buff));

	#define str "Enter number of lines needed to solve\r"

	printf("1(PID^ %d)'%s' readedd from socket\n", getpid(), str);

	send(sock, str, strlen(str), 0);
	recvBytes = recv(sock, &buff, sizeof(buff), 0);
	if(recvBytes < 0) error("ERROR reading from socket");
	printf("2(PID^ %d)'%s' size: %d recieved from socket\n", getpid(), buff, recvBytes);
	
	recvBytes = recv(sock, &buff, sizeof(buff), 0);
        if(recvBytes < 0) error("ERROR reading from socket");
        printf("2(PID^ %d)'%s' size: %d recieved from socket\n", getpid(), buff, recvBytes);


	num = atoi(buff);
	printf("3(PID^ %d)num: %d, code = %d\n", getpid(), num, (int)buff[0]);

	int n = solver(num, getpid());
	printf("4(PID^ %d)solver returned: %d\n", getpid(), n);
	bzero((char *)&buff, sizeof(buff));
	sprintf(buff, "%d\r", n);

	printf("5(PID^ %d)transmit to Client '%s'\n", getpid(), buff);
	//buff[strlen(buff)] = '\n';
	send(sock, buff, strlen(buff), 0);
	
	recvBytes = 0;
	recvBytes = recv(sock, &buff, sizeof(buff), 0);
	if(recvBytes < 0) error("ERROR reading from socket");
	printf("6(PID^ %d)'%s' size: %d recieved from socket\n", getpid(), buff, recvBytes);

	nclients--;
	printf("(PID^ %d)ended session with Client\n", getpid());
	printusers();
	
}

//---------------------------------------------------------------------------

int main(int argc, char *argv[]){
	perror("ERROR");
	if(argc == 2) printf("Server begin\nWaiting for Clients\n");
	else {
		printf("Please write port\n");
		exit(-1);
	}
	int sockfd, newsockfd;
	int portno;
	int pid;

	socklen_t clilen;
	struct sockaddr_in serv_addr, cli_addr;

	if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) error("ERROR opening socket"); 
	else printf("Server created tcp socket and ready to bind it\n");

	//these lines just for fun, try to uncomment and laugh
	//printf("Thees lines just for fun\n\nBIND IT! BIND IT! BIND IT! BIND IT!\n\n");
	//printf("___________§§§$$§§$§§_\n_________$$$$$§§§$$§§$§_\n_______($$$$$$$§§§$$§§$§§_(§§)_\n_______($$$$$$$$$$$$§§$$__$§§$)~\n________(§§§$$$$§$$$$§§$$§§$§§$§§~\n________((_($$§§$§§$§§§§§§§§§$$§§$\n________))_(§$$$$$$§§$$§§$§§$§§§§§~\n_________((_)_((§$$§$$§§$$§§$§§$§§§§((....\n___________))_)_§$$))_$$$§§§$$§§$§§$§§§§§\n___________(__(_))_§$§§§§§$$§§$$§§$§§$§§§\n_______________(__$_$$§§§§§§$$§§$$§§$§§$§§\n_______________)_$_$§$§§§§§§$§§$$§§$§§§§__§\n__________________)$__§§§§§§$§$$_§$$§§___§$§\n_________________(§___$§§§§§$§§$_§$$___§§§§§§\n_________________($§__$§$§§§$$$§§§_$__$§§§§§$§§\n__________________§§§_§$$$$$§§§§§§§$§§_§$$§$$§§§§\n___________________(§__§§§§§§§§§§$§§$$§§_§§$_§§§§§\n___________________(§_$$§§$§§§§§§§§$$§§$§_$§§§§§§\n____________________§_$§§$§§§§§§§§$$§§_§§$§§§§§§\n____________________§__$$§§§§$§§§§§§$_§§$§§$§§§\n_____________________§_$$§§§§$§§§§§__$§§$§§$§§\n_____________________§_$$_§§§§§§$§§$$§§$§§§§\n_____________________§§§§_§§§§§$§§$$§§$§§§§\n_____________________§§§$$::::§§$$$§§$$§§$_§\n___________________§§§§:§§§§§§__§§$§§$§_§§\n_________________§$$:§§§§§$$$§___§$§§$§§§§\n______________§§:§§$§$$§§$$__$§§$§§§§§§\n____________$$§:§§§$§$$$$$§§$$§§$§§$§_§§§\n__________$$§:§§$$$$$$§§$§§§§§§§$$___$§§\n_______$$$§::§$$$$$$§§$§§§§§§§§§______§§\n_____$$$§::§$$$$FL$§§§§§§§$§§$_________§\n___$$$§::§§$$$$§§§§$$$§§§§\n_$$§::§§§$$§§$§§§§§§§§§\n$§§§$§§§§§$$§§§§§§§\n§§§§§§§$$§§$§§§§\n_§§§§§§§§$§§_§§$\n_§§§§§§§§§$§§$_§§§\n__§§§§$§§§$§$§§$_§§\n___§§§§$§§§$§$§§$_§§\n_____$§§§§§§§§§§§$§_§§\n______§§§$§§§§§§$§§$§_§\n_______$§§§§§§§$$§$$§§_§§\n________§§§§$$§§$$§§$$§§_§\n_________§$§$$$§§§§§§$$§§$§§\n__________$$$$§§$_§§$§§$§§§§§§\n___________§$§§$§§_§§§§§§§§§$$§§$§\n____________§$§§$§§_§§§§§§§§_$$§§$\n_____________§$§§$§§_$§§§§§__§§$§\n_________________§$§§$______§§§§\n__________________§§§§§_§§__§§§§$\n__________________§§§$§§__$§§§§§§§\n___________________§$§§$§____§$§§§§\n_____________________§§§$§§___§§$§§\n\n");
	bzero((char*) &serv_addr, sizeof(serv_addr)); //make serv_addr full of zeros
	portno = atoi(argv[1]);

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portno);

	if(bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) error("ERROR on binding");
	else printf("Server bind socket and ready to listen\n");
	
	listen(sockfd, 5);
	clilen = sizeof(cli_addr);

	while(1){
		if((newsockfd = accept(sockfd, (struct sockaddr*)&cli_addr, & clilen)) < 0) error("ERROR on accept");
		nclients++;
		
		printusers();
		if((pid = fork()) < 0) error("ERROR on fork");
		if(pid == 0){
			close(sockfd);
			DOIT(newsockfd);
			break;
		} else close(newsockfd);
	}
	close(sockfd);
	return 0;
}

//---------------------------------------------------------------------------
