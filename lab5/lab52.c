#include <sys/types.h> 
#include <sys/ipc.h>
#include <sys/msg.h>
#include <errno.h>
#include <sys/stat.h> 
#include <wait.h> 
#include <fcntl.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h>
#include <string.h>
#define MSGSZ 2
//---------------------------------------------------------------------------

struct message{
	long mtype;
	char mesg[MSGSZ];
};

int main(){
	struct message msg[5];
	int i;
	int pid[5], msgid[5], total = 0;
	key_t key[5];
	printf("Begin\n");
	int n = 5;
	char buf[3];
	for (i = 0; i < n; i++){
		key[i] = 10 * i;
		if((msgid[i] = msgget(key[i], 0660 | IPC_CREAT)) != -1){
			printf("Queue %d with key %d was created\n", msgid[i], (int)key[i]);
		} else {
			printf("Queue with key %d was NOT created\n", (int)key[i]);
			exit(-1);
		}
		pid[i] = fork();
		sleep(1);
		if(pid[i] == 0){
			sprintf(buf, "%d", msgid[i]);
			if(execl("./lab51", "lab51", buf, NULL) < 0){
				printf("%d's errno = %s\n", i,  strerror(errno));
				printf("Error to start %d process\n", i);
				exit(-1);
			}
			else {
				printf("Processing started (pid = %d)\n", pid[i]);
			}
			
		}
		else {
			msg[i].mtype = 1;
			sprintf(msg[i].mesg, "%d", (i + 1));
			if(msgsnd(msgid[i], &msg[i], (strlen(msg[i].mesg)), 0) != -1){
				printf("Message '%s' to %d id was sended to child\n", msg[i].mesg, msgid[i]);
			} else {
				printf("Message '%s' to %d id was NOT sended to child\n", msg[i].mesg, msgid[i]);
			}
		}
	}
	for(i = 0; i < n; i++){
		if(msgrcv(msgid[i], &msg[i], MSGSZ, 2, 0) != -1){
			printf("Message '%s' with key %d, %d id was recieved from child\n", msg[i].mesg, (int)key[i], msgid[i]);
			total +=atoi(msg[i].mesg);
			if(msgctl(msgid[i], IPC_RMID, 0) != -1){
				printf("Queue with id %d was closed\n", msgid[i]);
			} else {
				printf("Queue with id %d was NOT closed\n", msgid[i]);
			}
		} else {
			printf("Message with key %d, %d id was NOT recieved from child\n", (int)key[i], msgid[i]);
		}
	}
	printf("Total = %d\n", total);
	return total;

}
//---------------------------------------------------------------------------
