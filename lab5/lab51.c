#include <unistd.h> 
#include <string.h>
#include <sys/types.h> 
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdio.h> 
#include <stdlib.h>
#include <fcntl.h>
#define MSGSZ 2

struct message{
	long mtype;
	char mesg[MSGSZ];
};

int main(int argc, char *argv[]){
	printf("(PID: %d)Begin\n", getpid());
	struct message msg;
	FILE *hdl1;
	FILE *hdl2;
	int msgid = atoi(argv[1]);
	printf("(PID: %d)Msgid = %d\n",getpid(), msgid);

	int num = 0;
	int total = 0;
	int matr[3];
	int vect[3];
	int i = 0;
	int stren = 0;

	if (argc < 2 ) {
		printf("Need more arguments to process!\n");
		exit (-1);
	}

	if(msgrcv(msgid, &msg, MSGSZ, 1, 0) != -1){
		printf("(PID: %d)Message '%s' with ID %d was recieved from parent\n", getpid(), msg.mesg,  msgid);
	} else {
		printf("(PID: %d)Message with ID %d was NOT recieved from parent\n", getpid(), msgid);
		exit(-1);
	}
	num = atoi(msg.mesg);
	printf("(PID: %d)Num = %d\n", getpid(), num);
	if ((hdl1 = fopen("matrix.txt", "r")) != NULL){
		while (!feof(hdl1)) {
			fscanf(hdl1, "%d %d %d", &matr[i], &matr[i + 1], &matr[i + 2]);
			if ((char)fgetc(hdl1) == 0x0A) stren++; 
			if (stren == num) break;
		}
		fclose(hdl1);
		if ((hdl2 = fopen("vector.txt", "r")) != NULL){
			while (!feof(hdl2)) {
				fscanf(hdl2, "%d\n%d\n%d\n", &vect[i], &vect[i + 1], &vect[i + 2]);
			}
			fclose(hdl2);
		}
		else {
			printf("Can't open file 2!\n");
			exit (-1);
		}
		total = 0;
		for (i = 0; i < 3; i++){
			total += matr[i] * vect[i];
		}
		printf("(PlD: %d)Result = %d\n", getpid(), total);
	}
	else {
		printf("Can't open file 1!\n");
		exit (-1);
	}
	msg.mtype = 2;
	sprintf(msg.mesg, "%d", total);
	if(msgsnd(msgid, &msg, strlen(msg.mesg), 0) != -1){
		printf("(PID: %d)Message '%s' with id %d was sended to parent\n", getpid(), msg.mesg, msgid);
	} else {
		printf("(PID: %d)Message '%s' with id %d was NOT sended to parent\n", getpid(), msg.mesg, msgid);
		exit(-1);
	}
	return 0;
}
//---------------------------------------------------------------------------
