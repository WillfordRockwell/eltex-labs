#include <stdlib.h>
#include <malloc.h>
#include <unistd.h>
#include <time.h>

int gameOver; //конец игры
const int width = 20; //ширина игрового поля
const int height = 20;//высота игрового поля
int x, y, fruitX, fruitY, score; //положения звейки, положения фруктов, счет
enum { STOP = 0, LEFT, RIGHT, UP, DOWN } dir; //определенное перечисление направления движений
int *xTail = NULL; //динамический массив, который хранит в себе координаты x хвоста
int *yTail = NULL; //динамический массив, который хранит в себе координаты y хвоста

int nTail; //длина хвоста

void NewFruit() {
	fruitX = 1 + rand() % (width - 1); //случайная X координата фрукта
	fruitY = 1 + rand() % (height - 1); //случайная Y координата фрукта
}

void Setup() {
	gameOver = 0; //игры мы в начале не проиграли
	dir = STOP; //изначально мы стоим
	x = width / 2; //стоим по середине ширины
	y = height / 2; //стоим по середине высоты
	NewFruit(); //создаем фрукт
	score = 0; //счет в начале нулевой
	nTail = 0; //длина хваоста еще 0
}

void Draw() {
	system("clear");
	for (int i = 0; i <= width; i++) printf("#"); //Рисуем верхнюю границу
	printf("\n"); //отрисовали

	for (int i = 1; i < height; i++) { //здесь мы рисуем саму змейку, фрукты и левую и правую границы
		printf("#");//левая граница
		for (int j = 1; j < width; j++) {
			if (i == fruitX && j == fruitY) printf("F");//фрукт
			else if (i == x && j == y) printf("O");//голова змейки
			else {
				int snaked = 0;
				if (nTail > 0) {
					for (int k = 0; k <= nTail; k++) {
						if (xTail[k] == i && yTail[k] == j) {
							printf("o"); //тело змеи
							snaked = 1;
						}
					}
				}
				if (!snaked)
					printf(" ");
			}
		}
		printf("#\n");//правая граница
	}

	for (int i = 0; i <= width; i++) printf("#"); //Рисуем нижнюю границу
	printf("\nScore: %d\n", score); //Выводим счет

}

void Input() {
	switch (getchar()) { //читаем какая именно
		case 'w':
			dir = UP;
			break;
		case 'a':
			dir = LEFT;
			break;
		case 's':
			dir = DOWN;
			break;
		case 'd':
			dir = RIGHT;
			break;
		case 'x':
			dir = STOP;
			gameOver = 1;
			break;
		default:
			break;
	}
}

void Logic() {
	int prevX, prevY, prev2X, prev2Y;
	if (nTail > 0) {
		prevX = xTail[0];
		prevY = yTail[0];
		xTail[0] = x, yTail[0] = y;
		for (int i = 0; i <= nTail; i++) {
			prev2X = xTail[i];
			prev2Y = yTail[i];
			xTail[i] = prevX;
			yTail[i] = prevY;
			prevX = prev2X;
			prevY = prev2Y;
		}
	}
		
	switch (dir){
		case STOP:
			break;
		case LEFT:
			y--;
			break;
		case RIGHT:
			y++;
			break;
		case UP:
			x--;
			break;
		case DOWN:
			x++;
			break;
		default:
			break;
	}

	if (x > width - 1 || x < 1 || y > height - 1 || y < 1) gameOver = 1;
	for (int i = 0; i < nTail; i++){
		if (x == xTail[i] && y == yTail[i]) {
			gameOver = 1;
			break;
		}
	}
	if (x == fruitX && y == fruitY) {
		score++;
		NewFruit();
		nTail++;
		xTail = (int*)realloc(xTail, (nTail + 1) * sizeof(int));
		yTail = (int*)realloc(yTail, (nTail + 1) * sizeof(int));
	}

}

int main() {
	system("/bin/stty raw");
	Setup();
	while (!gameOver) {
		Input();
		Logic();
		Draw();
		usleep(200000);
	}
	free(xTail);
	free(yTail);
	return 0;
}

